<?php

/**
 * Implements hook_drush_command().
 */
function versioncontrol_gitlab_drush_command() {
  return [
    'versioncontrol-gitlab-check-system-hooks' => [
      'description' => 'Checks that all system hooks are active.',
    ],
    'versioncontrol-gitlab-rotate-token' => [
      'description' => 'Rotate the personal access token used for API access.',
    ],
  ];
}

/**
 * Checks that all system hooks are active.
 */
function drush_versioncontrol_gitlab_check_system_hooks() {
  foreach (versioncontrol_gitlab_get_client()->api('system_hooks')->all() as $hook) {
    drush_log(dt('@alert_status @url', [
      '@alert_status' => $hook['alert_status'],
      '@url' => $hook['url'],
    ]));
    if ($hook['alert_status'] !== 'executable' || !empty($hook['disabled_until'])) {
      drush_set_error(dt('Hook is not executable or disabled!'));
    }
  }
}

/**
 * Rotate the personal access token used for API access.
 */
function drush_versioncontrol_gitlab_rotate_token() {
  $pat_info = versioncontrol_gitlab_pat_info();
  $update = json_decode(versioncontrol_gitlab_get_client()->getHttpClient()->post('personal_access_tokens/' . $pat_info->id . '/rotate?' . http_build_query([
    'expires_at' => (new DateTimeImmutable('+2 weeks'))->format('Y-m-d'),
  ]))->getBody());
  variable_set('versioncontrol_gitlab_key', $update->token);
}
