<?php

/**
 * Pings the DB during long paging operations to avoid disconnects.
 */
class VersioncontrolGitlabResultPager extends Gitlab\ResultPager {
  private $time;

  /**
   * {@inheritdoc}
   */
  public function fetchAll(Gitlab\Api\ApiInterface $api, $method, array $parameters = []) {
    $this->time = time();
    return parent::fetchAll($api, $method, $parameters);
  }

  /**
   * {@inheritdoc}
   */
  public function hasNext() {
    if (time() - $this->time > 60) {
      if (function_exists('drush_log')) {
        $caller = debug_backtrace()[2];
        drush_log(dt('Pinging the DB in @caller', ['@caller' => $caller['file'] . ':' . $caller['line']]), Drush\Log\LogLevel::INFO);
      }
      db_query('SELECT 1');
      $this->time = time();
    }

    return parent::hasNext();
  }

}
