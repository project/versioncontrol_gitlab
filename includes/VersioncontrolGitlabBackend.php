<?php

class VersioncontrolGitlabBackend extends VersioncontrolGitBackend {

  public $type = 'gitlab';

  public $classesEntities = [
    'repo' => 'VersioncontrolGitlabRepository',
    'account' => 'VersioncontrolGitlabAccount',
    'operation' => 'VersioncontrolGitOperation',
    'item' => 'VersioncontrolGitItem',
    'event' => 'VersioncontrolGitEvent',
  ];

  public $classesControllers = [
    'repo' => 'VersioncontrolGitlabRepositoryController',
    'operation' => 'VersioncontrolGitOperationController',
    'item' => 'VersioncontrolGitItemController',
    'event' => 'VersioncontrolGitEventController',
  ];

  public function __construct() {
    parent::__construct();
    $this->name = 'Gitlab';
    $this->description = t('Integrates with Gitlab');
  }

  /**
   * Provide default plugins.
   */
  public function getDefaultPluginName($plugin_type) {
    if ($plugin_type == 'reposync') {
      return 'gitlab_history';
    }
    if ($plugin_type == 'webviewer_url_handler') {
      return 'gitlab';
    }
    if ($plugin_type == 'auth_handler') {
      return 'gitlabaccount';
    }
  }

}
