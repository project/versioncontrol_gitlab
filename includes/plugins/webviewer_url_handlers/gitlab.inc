<?php
/**
 * @file
 * This plugin provides support for GitLab.
 */

$plugin = [
  'vcs' => 'gitlab',
  'title' => t('Gitlab URL autogenerator'),
  'url_templates' => [
    // /drupal
    'repository_view' => '%base_url/%repo_name',
    // /drupal/commit/6a8fe612750b174fa8bb9689dff16b300f981efc
    'commit_view' => '%base_url/%repo_name/commit/%revision',
    // /drupal/commits/58d601eb0a94ae40a4b794caf47f91c096c10f33/core/config/schema/core.entity.schema.yml
    'log_view' => '%base_url/%repo_name/commits/%revision%path',
    // /drupal/blob/02f932546662854f23e5eacd4a905e80bf7c6697/core/config/schema/core.entity.schema.yml
    'item_view' => '%base_url/%repo_name/blob/%revision%path',
  ],
  'handler' => [
    'class' => 'VersioncontrolRepositoryUrlHandlerGitlab',
    'file' => 'VersioncontrolRepositoryUrlHandlerGitlab.inc',
    'path' => drupal_get_path('module', 'versioncontrol_gitlab') . '/includes/plugins/webviewer_url_handlers',
    'parent' => 'none',
  ],
];
