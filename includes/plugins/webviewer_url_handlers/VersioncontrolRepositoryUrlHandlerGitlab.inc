<?php

/**
 * Repository Url handler class for GitLab.
 */
class VersioncontrolRepositoryUrlHandlerGitlab extends VersioncontrolRepositoryUrlHandler {

  public function __construct($repository, $base_url, $template_urls) {
    parent::__construct($repository, $base_url, $template_urls);

    // Add namespace to base URL.
    if (!empty($this->repository->namespace)) {
      $this->baseUrl .= '/' . $this->repository->namespace;
    }
  }

  public function getRepositoryViewUrl($label = NULL) {
    $url = parent::getRepositoryViewUrl($label);

    if (is_object($label)) {
      $url .= '/tree/' . drupal_encode_path($label->name);
    }

    return $url;
  }

  public function getItemLogViewUrl($item, $current_label = NULL) {
    return strtr($this->getTemplateUrl('log_view'), [
      '%repo_name' => $this->repository->name,
      '%path' => $item->path,
      '%revision' => $item->revision,
    ]);
  }

  public function getItemViewUrl($item, $current_label = NULL) {
    if ($item->type == VERSIONCONTROL_ITEM_FILE_DELETED || $item->type == VERSIONCONTROL_ITEM_DIRECTORY_DELETED) {
      // Do not link to deleted items.
      return '';
    }

    return strtr($this->getTemplateUrl('item_view'), [
      '%repo_name' => $this->repository->name,
      '%path' => $item->path,
      '%revision' => $item->revision,
    ]);
  }

}
