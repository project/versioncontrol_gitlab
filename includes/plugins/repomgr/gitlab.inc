<?php

$plugin = array(
    'title' => t('Default Gitlab repository manager'),
    'worker' => array(
        'class' => 'VersioncontrolGitlabRepositoryManagerWorkerDefault',
    ),
);