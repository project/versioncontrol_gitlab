<?php

class VersioncontrolGitlabRepositoryManagerWorkerDefault implements VersioncontrolGitRepositoryManagerWorkerInterface {

  protected $repository;

  public function setRepository(VersioncontrolRepository $repository) {
    // Additional parameter check to the appropriate Git subclass of that
    // required by the interface itself.
    if (!$repository instanceof VersioncontrolGitRepository) {
      $msg = 'The repository "@name" with repo_id "@repo_id" passed to ' . __METHOD__ . ' was not a VersioncontrolGitlabRepository instance.';
      $vars = array(
        '@name' => $repository->name,
        '@repo_id' => empty($repository->repo_id) ? '[NEW]' : $repository->repo_id,
      );
      watchdog('versioncontrol_gitlab', $msg, $vars, WATCHDOG_ERROR);
      throw new Exception(strtr($msg, $vars), E_ERROR);
    }
    $this->repository = $repository;
  }

  public function create() {
    $this->init();
    $this->save();
  }

  public function init($_import_url = FALSE) {
    try {
      $data = [
        'path' => $this->repository->name,
        'description' => $this->repository->description,
        'request_access_enabled' => FALSE,
        'namespace_id' => variable_get('versioncontrol_gitlab_namespace_ids', [])[$this->repository->namespace],
      ];
      if ($_import_url) {
        $data['import_url'] = $_import_url;
      }
      drupal_alter('versioncontrol_gitlab_project_data', $data, $this->repository);
      $gitlab_project = versioncontrol_gitlab_get_client()->api('projects')->create($this->repository->name, $data);
      $this->repository->gitlab_project_id = $gitlab_project['id'];

    }
    catch (Exception $e) {
      $this->handleException($e, 'repository initialization');
    }

    $this->updateIssueService();

    return TRUE;
  }

  /**
   * Import a VersioncontrolGitRepository into GitLab.
   *
   * Records the GitLab project ID and namespace.
   *
   * @param string $import_url
   *   The Git remote URL to import from.
   */
  public function import($import_url) {
    $this->init($import_url);

    db_merge('versioncontrol_gitlab_repositories')
      ->key(['repo_id' => $this->repository->repo_id])
      ->fields([
        'gitlab_project_id' => $this->repository->gitlab_project_id,
        'namespace' => $this->repository->namespace,
      ])
      ->execute();
  }

  /**
   * Not supported for GitLab.
   */
  public function reInit(array $flush) {
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    $this->repository->save();
    return TRUE;
  }

  /**
   * Not supported for GitLab.
   */
  public function configSet($name, $value, $type = NULL) {
  }

  public function delete() {
    $this->repository->getApiClient()->api('projects')->remove($this->repository->gitlab_project_id);

    $this->repository->delete();
    return TRUE;
  }

  public function move($name) {
    //This will never be called.
  }

  /**
   * Update GitLab project configuration.
   *
   * @param array $conf
   *   Project attributes to update, see
   *   https://docs.gitlab.com/ee/api/projects.html#edit-project.
   */
  public function updateProject(array $conf) {
    try {
      $this->repository->getApiClient()->api('projects')->update($this->repository->gitlab_project_id, $conf);
    }
    catch (Exception $e) {
      $this->handleException($e, 'update project');
    }
  }

  /**
   * Transfer the GitLab project to a new namespace.
   *
   * Also update the project’s name to match the local repository object.
   *
   * @param string $namespace
   *   The new namespace’s name.
   */
  public function transferAndUpdateName($namespace) {
    $this->repository->namespace = $namespace;

    try {
      $this->repository->getApiClient()->api('projects')->update($this->repository->gitlab_project_id, [
        'name' => $this->repository->name,
        'path' => $this->repository->name,
      ]);
      $this->repository->getApiClient()->api('projects')->transfer($this->repository->gitlab_project_id, variable_get('versioncontrol_gitlab_namespace_ids', [])[$this->repository->namespace]);
    }
    catch (Exception $e) {
      $this->handleException($e, 'transfer and update name');
    }

    $this->updateIssueService();
  }

  /**
   * Update issue tracker service to point to the Drupal site’s project.
   */
  public function updateIssueService() {
    try {
      if (!empty($this->repository->project_nid) && ($project_node = node_load($this->repository->project_nid))) {
        $project_wrapper = entity_metadata_wrapper('node', $project_node);
        $this->repository->getApiClient()->getHttpClient()->put('projects/' . $this->repository->gitlab_project_id . '/integrations/custom-issue-tracker', ['Content-Type' => 'application/json'], json_encode([
          'project_url' => url('node/' . $project_node->nid, ['absolute' => TRUE]),
          'issues_url' => url('i', ['absolute' => TRUE]) . '/:id',
          'new_issue_url' => url('node/add/project-issue/' . $project_wrapper->field_project_machine_name->value(), ['absolute' => TRUE]),
          'use_inherited_settings' => 0,
        ]));
      }
    }
    catch (Exception $e) {
      $this->handleException($e, 'update issue service');
    }
  }

  public function setDescription($description) {
    $id = $this->repository->gitlab_project_id;
    $this->repository->getApiClient()->api('projects')->update($id, ['description' => $description]);
  }

  public function setDefaultBranch($branch_name) {
    try {
      versioncontrol_gitlab_get_client()->api('projects')->update($this->repository->gitlab_project_id, ['default_branch' => $branch_name]);
      $this->repository->defaultBranch = $branch_name;
    }
    catch (Exception $e) {
      $this->handleException($e, 'default branch update');
    }
  }

  public function fetchDefaultBranch() {
    $this->repository->defaultBranch = $this->repository->getApiClient()->api('projects')->show($this->gitlab_project_id)['default_branch'];
    return $this->repository->defaultBranch;
  }

  /**
   * Ensure we're properly set up before we try to do anything. If setup does
   * not pass verification, watchdog and optionally throw exceptions.
   */
  public function verify($exception = TRUE) {
    if (!is_null($this->verified)) {
      return $this->verified;
    }

    $this->verified = TRUE;

    if (empty($this->repository)) {
      $this->verified = FALSE;
      $msg = 'No repository object was attached for the repomgr to work on.';
      watchdog('versioncontrol_gitlab', $msg, array(), WATCHDOG_ERROR);
      if ($exception) {
        throw new Exception($msg, E_ERROR);
      }
    }

    return $this->verified;
  }

  public function setUserAuthData($uid, $auth_data) {
    $auth_handler = $this->repository->getAuthHandler();
    // special case, let the caller init multiple users' auth data at once
    if (is_array($uid)) {
      foreach ($uid as $id) {
        $auth_handler->setUserData($id, $auth_data);
      }
    }
    else {
      $auth_handler->setUserData($uid, $auth_data);
    }
    $auth_handler->save();
  }

  public function forceSyncUsers() {
    try {
      $this->repository->getAuthHandler()->syncMembers();
    }
    catch (Exception $e) {
      $this->handleException($e, 'sync users');
    }
  }

  /**
   * Log and throw an exception with additional context.
   *
   * @param Exception $e
   *   The exception being intercepted.
   * @param string $where
   *   Where this is happening.
   *
   * @throws Exception
   */
  private function handleException(Exception $e, $where) {
    $string = 'Exception in @where for %name (%id): %message';
    $args = [
      '@where' => $where,
      '%name' => $this->repository->name,
      '%id' => $this->repository->repo_id,
      '%message' => $e->getMessage(),
    ];
    watchdog('versioncontrol_gitlab', $string, $args, WATCHDOG_ERROR);
    throw new Exception(format_string($string, $args), E_ERROR);
  }

}
