<?php

$plugin = [
  'title' => t('Gitlab history synchronizer'),
  'worker' => [
    'class' => 'VersioncontrolGitlabRepositoryHistorySynchronizer',
  ],
];
