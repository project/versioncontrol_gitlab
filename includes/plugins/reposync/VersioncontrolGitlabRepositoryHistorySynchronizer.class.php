<?php

class VersioncontrolGitlabRepositoryHistorySynchronizer extends VersioncontrolGitRepositoryHistorySynchronizerDefault {

  /**
   * {@inheritdoc}
   */
  protected function parseAndInsertCommit($hash, $branches, $tags, $options) {
    $res = $this->repository->fetchCommit($hash);
    $merge = count($res['parent_ids']) > 1;
    $op = new VersioncontrolGitOperation($this->repository->getBackend());
    $op->build([
      'type' => VERSIONCONTROL_OPERATION_COMMIT,
      'revision' => $res['id'],
      'author' => $res['author_email'],
      'author_name' => $res['author_name'],
      'committer' => $res['committer_email'],
      'committer_name' => $res['committer_name'],
      'parent_commit' => isset($res['parent_ids'][0]) ? $res['parent_ids'][0] : '',
      'merge' => $merge,
      'author_date' => strtotime($res['authored_date']),
      'committer_date' => strtotime($res['committed_date']),
      'message' => $res['message'],
      'repository' => $this->repository,
    ]);
    $op->labels = $this->getCommitLabels($res['id'], $branches, $tags, $options);
    $op->insert(['map users' => TRUE]);

    // Create VersioncontrolGitItem objects.
    $op->itemRevisions = $this->parseItems($dummy, $dummy, [
      // Pass backend in data array to avoid just another param.
      'backend' => $this->repository->getBackend(),
      'repository' => $this->repository,
      'vc_op_id' => $op->vc_op_id,
      'type' => VERSIONCONTROL_ITEM_FILE,
      'revision' => $res['id'],
      'action' => $merge ? VERSIONCONTROL_ACTION_MERGED : VERSIONCONTROL_ACTION_MODIFIED,
    ], $res['parent_ids']);
    $op->save(['nested' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getCommitLabels($revision, $branch_label_list, $tag_label_list, $options) {
    $labels = [];

    $commit_refs = $this->repository->getApiPager()->fetchall($this->repository->getApiClient()->api('repositories'), 'commitRefs', [$this->repository->gitlab_project_id, $revision, ['per_page' => 100]]);
    // Process branches.
    if ($options['operation_labels_mapping'] & VersioncontrolRepositoryHistorySynchronizerInterface::MAP_COMMIT_BRANCH_RELATIONS) {
      foreach ($commit_refs as $ref) {
        if ($ref['type'] === 'branch' && !empty($branch_label_list[$ref['name']])) {
          $labels[] = $branch_label_list[$ref['name']];
        }
      }
    }
    // Process tags.
    if ($options['operation_labels_mapping'] & VersioncontrolRepositoryHistorySynchronizerInterface::MAP_COMMIT_TAG_RELATIONS) {
      foreach ($commit_refs as $ref) {
        if ($ref['type'] === 'tag' && !empty($tag_label_list[$ref['name']])) {
          $labels[] = $tag_label_list[$ref['name']];
        }
      }
    }

    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  protected function parseItems(&$logs, &$line, $data, $parents) {
    static $blob_hashes = [];
    $op_items = [];
    $repositories_api = $this->repository->getApiClient()->api('repositories');

    // Find changed files.
    $pager = $this->repository->getApiPager();
    foreach ($this->fetchDiff($data['revision']) as $diff) {
      $path = '/' . $diff['new_path'];
      $op_items[$path] = new VersioncontrolGitItem($data['backend']);
      $data['path'] = $path;
      $op_items[$path]->build($data);
      unset($data['path']);
      $op_items[$path]->line_changes_added = $diff['lines_added'];
      $op_items[$path]->line_changes_removed = $diff['lines_removed'];

      if ($diff['new_file']) {
        $op_items[$path]->action = VERSIONCONTROL_ACTION_ADDED;
      }
      else {
        // Fill in the source_items for non-added items.
        $this->fillSourceItem($op_items[$path], $parents, $data + ['old_path' => $diff['old_path']]);
      }

      if ($diff['deleted_file']) {
        $op_items[$path]->action = VERSIONCONTROL_ACTION_DELETED;
        $op_items[$path]->action = VERSIONCONTROL_ITEM_FILE_DELETED;
      }
      else {
        $directory = preg_replace('#(^|/)[^/]*$#', '', $diff['new_path']);
        if (!isset($blob_hashes[$data['revision']][$directory])) {
          // Keep memory usage under control.
          if (count($blob_hashes) > 1000) {
            $blob_hashes = array_slice($blob_hashes, 500, NULL, TRUE);
          }

          $blob_hashes[$data['revision']][$directory] = array_column($pager->fetchall($repositories_api, 'tree', [$this->repository->gitlab_project_id, [
            'ref' => $data['revision'],
            'path' => $directory,
            'per_page' => 100,
          ]]), 'id', 'path');
        }
        $op_items[$path]->blob_hash = $blob_hashes[$data['revision']][$directory][$diff['new_path']];
      }
    }

    return $op_items;
  }

  /**
   * {@inheritdoc}
   */
  protected function fillSourceItem(&$item, $parents, $inc_data) {
    // Figure out which parent an item was revised in.
    if (count($parents) === 1) {
      // If there is one parent, that’s it.
      $parent = $parents[0];
    }
    elseif (count($parents) > 1) {
      // Otherwise, look through the diff of each parent to match up on path.
      foreach ($parents as $sha) {
        foreach ($this->fetchDiff($sha) as $diff) {
          if ($diff['new_path'] === $inc_data['old_path']) {
            $parent = $sha;
            break 2;
          }
        }
      }
    }

    if (isset($parent)) {
      $source_item = new VersioncontrolGitItem($item->getBackend());
      $source_item->build([
        'type' => VERSIONCONTROL_ITEM_FILE,
        'repository' => $inc_data['repository'],
        'path' => '/' . $inc_data['old_path'],
        'revision' => $parent,
      ]);
      $item->setSourceItem($source_item);
    }
  }

  /**
   * Fetch a diff for a commit from GitLab.
   *
   * @param string $commit
   *   A Git commit hash.
   *
   * @return array
   *   GitLab API response, with 'diff' replaced by 'lines_added' and
   *   'lines_removed'.
   */
  private function fetchDiff($commit) {
    static $cache = [];

    if (!isset($cache[$commit])) {
      // Keep memory usage under control.
      if (count($cache) > 1000) {
        $cache = array_slice($cache, 500, NULL, TRUE);
      }

      $cache[$commit] = $this->repository->getApiPager()->fetchall($this->repository->getApiClient()->api('repositories'), 'diff', [$this->repository->gitlab_project_id, $commit]);
      foreach ($cache[$commit] as &$diff) {
        $diff['lines_added'] = preg_match_all('/^\+/m', $diff['diff']);
        $diff['lines_removed'] = preg_match_all('/^-/m', $diff['diff']);
        unset($diff['diff']);
      }
    }

    return $cache[$commit];
  }

  /**
   * {@inheritdoc}
   */
  function processTags($tags_new) {
    // Get a list of all tag names with the corresponding commit.
    $tag_commit_list = $this->getTagCommitList($tags_new);
    foreach ($tag_commit_list as $tag_name => $tag_commit) {
      $tag = $this->repository->getBackend()->buildEntity('tag', [
        'name' => $tag_name,
        'repository' => $this->repository,
        'repo_id' => $this->repository->repo_id,
        'action' => VERSIONCONTROL_ACTION_ADDED,
      ]);
      $tag->insert();
    }
  }

  /**
   * {@inheritdoc}
   */
  function getTagCommitList($tags) {
    return array_intersect_key(array_column($this->repository->getApiPager()->fetchall($this->repository->getApiClient()->api('repositories'), 'tags', [$this->repository->gitlab_project_id, ['per_page' => 100]]), 'target', 'name'), $tags);
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitInterval($start, $end) {
    if ($start == GIT_NULL_REV) {
      // Start as null rev is the same as saying "all revs back to the root".
      return array_reverse(array_column($this->repository->getApiPager()->fetchall($this->repository->getApiClient()->api('repositories'), 'commits', [$this->repository->gitlab_project_id, ['ref_name' => $end, 'per_page' => 100]]), 'id'));
    }
    elseif ($end == GIT_NULL_REV) {
      // null rev as the end of a rev list makes no sense.
      throw new VersioncontrolSynchronizationException("Attempted to git rev-list with the null rev as the end commit.", E_RECOVERABLE_ERROR);
    }
    else {
      // Commits between two revisions.
      return array_column($this->repository->getApiClient()->api('repositories')->compare($this->repository->gitlab_project_id, $start, $end)['commits'], 'id');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncEvent(VersioncontrolEvent $event, $options) {
    $return = parent::syncEvent($event, $options);

    // Set the default branch if needed.
    if (!$this->repository->defaultBranch) {
      $this->repository->defaultBranch = $this->getDefaultBranch();
      $this->repository->save(['nested' => FALSE]);
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepare() {
    return $this->repository->lock();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultBranch() {
    return $this->repository->getApiClient()->api('projects')->show($this->repository->gitlab_project_id)['default_branch'];
  }

}
