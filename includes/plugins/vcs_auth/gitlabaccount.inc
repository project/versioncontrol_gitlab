<?php
/**
 * @file
 *   Defines account vcs_auth plugin.
 */
$plugin = array(
  'title' => t('GitLab Account (restricted write access)'),
  'handler' => array(
    'class' => 'VersioncontrolGitlabAuthHandlerMappedAccounts',
  ),
);
