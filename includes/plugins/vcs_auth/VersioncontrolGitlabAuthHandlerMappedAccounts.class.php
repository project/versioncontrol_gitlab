<?php

class VersioncontrolGitlabAuthHandlerMappedAccounts extends VersioncontrolAuthHandlerMappedAccounts {

  /**
   * Permission value indicating VersioncontrolAuthHandlerMappedAccounts::ALL,
   * plus granting access as a GitLab project maintainer.
   */
  const ALL_MAINTAINER = 40;

  /**
   * Sync all VCS maintainers of a project.
   *
   * @throws Exception
   */
  public function syncMembers() {
    $client = versioncontrol_gitlab_get_client();
    $projects_api = $client->api('projects');

    // Get users who should have access.
    $this->build();
    $uid_access = array_filter(array_column($this->userData, 'access', 'uid'));
    foreach ($uid_access as &$level) {
      if ($level == VersioncontrolGitlabAuthHandlerMappedAccounts::ALL) {
        // 30 is default “Developer” access
        // https://docs.gitlab.com/ee/api/members.html
        $level = 30;
      }
    }
    $uid_to_gitlab = array_filter(array_map('versioncontrol_gitlab_get_user_id', user_load_multiple(array_keys($uid_access))));
    $expected_access = [];
    foreach ($uid_to_gitlab as $uid => $gitlab_user_id) {
      $expected_access[$gitlab_user_id] = $uid_access[$uid];
    }

    // Get current maintainers in GitLab.
    $pager = new \Gitlab\ResultPager($client);
    $gitlab_access = array_column($pager->fetchall($projects_api, 'members', [$this->repository->gitlab_project_id]), 'access_level', 'id');

    // Remove people without access.
    foreach (array_diff_key($gitlab_access, $expected_access) as $gitlab_user_id => $access_level) {
      try {
        $projects_api->removeMember($this->repository->gitlab_project_id, $gitlab_user_id);
      }
      catch (Exception $e) {
        watchdog('versioncontrol_gitlab', '@type removing GitLab user ID @gitlab_user_id maintainership for repository @repository: %message', [
          '@type' => get_class($e),
          '@gitlab_user_id' => $gitlab_user_id,
          '@repository' => $this->repository->name,
          '%message' => $e->getMessage(),
        ], WATCHDOG_WARNING);
      }
    }

    // Add people with access.
    foreach (array_diff_key($expected_access, $gitlab_access) as $gitlab_user_id => $access_level) {
      try {
        $projects_api->addMember($this->repository->gitlab_project_id, $gitlab_user_id, (string) $access_level);
      }
      catch (Exception $e) {
        watchdog('versioncontrol_gitlab', '@type adding GitLab user ID @gitlab_user_id maintainership for repository @repository: %message', [
          '@type' => get_class($e),
          '@gitlab_user_id' => $gitlab_user_id,
          '@repository' => $this->repository->name,
          '%message' => $e->getMessage(),
        ], WATCHDOG_WARNING);
      }
    }

    // Adjust access level.
    foreach (array_intersect_key($expected_access, $gitlab_access) as $gitlab_user_id => $access_level) {
      // Lower access levels can be granted via GitLab, leave those in place.
      // Only elevate access to maintainer.
      if ($access_level == self::ALL_MAINTAINER && $gitlab_access[$gitlab_user_id] < $access_level) {
        try {
          $projects_api->saveMember($this->repository->gitlab_project_id, $gitlab_user_id, (string) $access_level);
        }
        catch (Exception $e) {
          watchdog('versioncontrol_gitlab', '@type updating access level for GitLab user ID @gitlab_user_id maintainership for repository @repository: %message', [
            '@type' => get_class($e),
            '@gitlab_user_id' => $gitlab_user_id,
            '@repository' => $this->repository->name,
            '%message' => $e->getMessage(),
          ], WATCHDOG_WARNING);
        }
      }
    }
  }

}
