<?php

/**
 * @class
 * Extends VersioncontrolGitRepositoryController with GitLab-specific features.
 */
class VersioncontrolGitlabRepositoryController extends VersioncontrolGitRepositoryController {

  /**
   * Extends the base query with the GitLab backend’s additional data in
   * {versioncontrol_gitlab_repositories}.
   *
   * @return SelectQuery
   */
  protected function buildQueryBase($ids, $conditions) {
    $query = parent::buildQueryBase($ids, $conditions);
    $alias = $query->leftJoin('versioncontrol_gitlab_repositories', 'vc_gl_r', 'base.repo_id = vc_gl_r.repo_id');
    $query->fields($alias, drupal_schema_fields_sql('versioncontrol_gitlab_repositories'));
    return $query;
  }

}
