<?php

use \Gitlab\Exception;

class VersioncontrolGitlabRepository extends VersioncontrolGitRepository {

  /**
   * Numeric ID for this repository’s GitLab project.
   */
  public $gitlab_project_id;

  /**
   * GitLab namespace the GitLab project belongs to.
   */
  public $namespace;

  /**
   * Get a GitLab API client.
   */
  public function getApiClient() {
    static $api;

    if (is_null($api)) {
      $api = versioncontrol_gitlab_get_client();
    }

    return $api;
  }

  /**
   * Get a Gitlab\ResultPager instance.
   */
  public function getApiPager() {
    static $pager;

    if (is_null($pager)) {
      $pager = new VersioncontrolGitlabResultPager($this->getApiClient());
    }

    return $pager;
  }

  protected function backendInsert($options) {
    parent::backendInsert($options);

    db_insert('versioncontrol_gitlab_repositories')
      ->fields([
        'repo_id' => $this->repo_id,
        'gitlab_project_id' => $this->gitlab_project_id,
        'namespace' => $this->namespace,
      ])
      ->execute();
  }

  protected function backendUpdate($options) {
    parent::backendUpdate($options);

    db_update('versioncontrol_gitlab_repositories')
      ->condition('repo_id', $this->repo_id)
      ->fields([
        'gitlab_project_id' => $this->gitlab_project_id,
        'namespace' => $this->namespace,
      ])
      ->execute();
  }

  protected function backendDelete($options) {
    parent::backendDelete($options);

    db_delete('versioncontrol_gitlab_repositories')
      ->condition('repo_id', $this->repo_id)
      ->execute();
  }

  public function finalizeEvent(VersioncontrolEvent $event) {
    // Check event, so we process only git events.
    if (!$event instanceof VersioncontrolGitEvent) {
      $msg = t('An incompatible VersioncontrolEvent object (of class @class) was provided to a Git repository synchronizer for event-driven repository synchronization.', array('@class' => get_class($event)));
      throw new InvalidArgumentException($msg, E_ERROR);
    }

    foreach ($event as $ref) {
      $ref->ff = (int) $this->isFastForward($ref->old_sha1, $ref->new_sha1);

      if (!$ref->eventCreatedMe()) {
        // See if we can attach a label from the db to this refchange.
        $ref->syncLabel();
      }
    }
  }

  /**
   * Check if a commit is a fast-forward.
   *
   * @param string $old_commit
   *   The commit hash this label is being moved from.
   * @param string $new_commit
   *   The commit hash this label will point at, if allowed.
   *
   * @return bool
   *   TRUE if this is a fast-forward commit.
   */
  public function isFastForward($old_commit, $new_commit) {
    if (GIT_NULL_REV === $old_commit || GIT_NULL_REV === $new_commit) {
      // Adding or removing refs is considered a ff.
      return TRUE;
    }

    try {
      $merge_base = $this->getApiClient()->api('repositories')->mergeBase($this->gitlab_project_id, [$old_commit, $new_commit]);
      if (empty($merge_base['id'])) {
        return FALSE;
      }
      return $merge_base['id'] === $old_commit;
    }
    catch (RuntimeException $e) {
      if ($e->getCode() === 404) {
        // Merge base did not find a common ancestor.
        return FALSE;
      }
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fetchBranches() {
    $branches = [];

    foreach ($this->getApiPager()->fetchall($this->getApiClient()->api('repositories'), 'branches', [$this->gitlab_project_id, ['per_page' => 100]]) as $branch_info) {
      $branches[$branch_info['name']] = new VersioncontrolBranch($this->backend);
      $branches[$branch_info['name']]->build([
        'repo_id' => $this->repo_id,
        'label_id' => NULL,
        'name' => $branch_info['name'],
        'tip' => $branch_info['commit']['id'],
      ]);
    }

    return $branches;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchBranch($name) {
    try {
      $result = $this->getApiClient()->api('repositories')->branch($this->gitlab_project_id, $name);
      return $this->getBackend()->buildEntity('branch', [
        'repo_id' => $this->repo_id,
        'label_id' => NULL,
        'tip' => $result['commit']['id'],
        'name' => $result['name'],
      ]);
    }
    catch (RuntimeException $e) {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fetchTags() {
    $tags = [];
    foreach ($this->getApiPager()->fetchall($this->getApiClient()->api('repositories'), 'tags', [$this->gitlab_project_id, ['per_page' => 100]]) as $tagdata) {
      $tags[$tagdata['name']] = new VersioncontrolTag();
      $tags[$tagdata['name']]->build([
        'repo_id' => $this->repo_id,
        'label_id' => NULL,
        'tip' => $tagdata['commit']['id'],
        'name' => $tagdata['name'],
      ]);
    }
    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchTag($name) {
    try {
      $result = $this->getApiClient()->api('tags')->show($this->gitlab_project_id, $name);
      return $this->getBackend()->buildEntity('tag', [
        'repo_id' => $this->repo_id,
        'label_id' => NULL,
        'tip' => $result['target'],
        'name' => $result['name'],
      ]);
    }
    catch (RuntimeException $e) {
      return FALSE;
    }
  }

  /**
   * Delete a tag.
   */
  public function deleteTag($name) {
    return $this->getApiClient()->api('tags')->remove($this->gitlab_project_id, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchCommits($branch_name = NULL) {
    if (is_null($branch_name)) {
      $parameters = ['all' => TRUE];
    }
    else {
      $parameters = ['ref_name' => $branch_name];
    }
    $parameters['per_page'] = 100;

    $commits = $this->getApiPager()->fetchall($this->getApiClient()->api('repositories'), 'commits', [$this->gitlab_project_id, $parameters]);
    $commit_ids = array_column($commits, 'id');

    if (is_null($branch_name)) {
      $this->all_commits = array_combine($commit_ids, $commits);
      if (function_exists('drush_log')) {
        drush_log(dt('Found @count commits', ['@count' => count($this->all_commits)]), Drush\Log\LogLevel::INFO);
      }
      return $commit_ids;
    }
    else {
      return array_reverse($commit_ids);
    }
  }

  /**
   * Fetch a single commit from GitLab.
   *
   * @param string $hash
   *   A Git commit hash.
   *
   * @return array
   *   GitLab API response.
   */
  public function fetchCommit($hash) {
    if (isset($this->all_commits[$hash])) {
      return $this->all_commits[$hash];
    }

    return $this->getApiClient()->api('repositories')->commit($this->gitlab_project_id, $hash);
  }

  /**
   * Executing shell commands is bad.
   */
  public function exec($command) {
    throw new VersioncontrolSynchronizationException('Attempted executing command, which should not happen: ' . $command);
  }

  /**
   * {@inheritdoc}
   */
  public function isValidGitRepo() {
    try {
      return !empty($this->getApiClient()->api('projects')->show($this->gitlab_project_id));
    }
    catch (RuntimeException $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get the Git remote URL for the repository.
   *
   * @param string $type
   *   Either 'http' or 'ssh'.
   *
   * @return string
   *   The Git remote URL for the repository.
   */
  public function remoteUrl($type = 'http') {
    switch ($type) {
      case 'http':
        return variable_get('versioncontrol_gitlab_url') . '/' . $this->namespace . '/' . $this->name . '.git';

      case 'ssh':
        return 'git@' . variable_get('versioncontrol_gitlab_url_ssh') . ':' . $this->namespace . '/' . $this->name . '.git';
    }
  }

}
